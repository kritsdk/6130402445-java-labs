package hompan.kritsadakorn.lab4;

public interface Refuelable {
	
	public void refuel();
}
