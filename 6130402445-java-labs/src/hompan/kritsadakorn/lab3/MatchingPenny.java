package hompan.kritsadakorn.lab3;
/**
 * MatchingPenny
 * 
 Author: Kritsadakorn Hompan
 ID: 613040244-5
 Sec: 1
 Date: February 3, 2019
 *
 **/

import java.util.Random;
import java.util.Scanner;

public class MatchingPenny {
    public static void main(String[] args) {
        Random random = new Random();
        int random_HT = random.nextInt(2);
        String random1;
		if (random_HT == 0) {
        	random1 = "head";
        }
        else {
        	random1 = "tail";
        }
		
    	while (true) {
        Scanner sc = new Scanner(System.in);
        System.out.print("Enter Head or Tail : ");
        String input = sc.nextLine();
        String w = input.toLowerCase();
        if (w.equals("exit") ) {
        	System.out.println("Good Bye");
        	System.exit(0);
        }
        
        else if (!w.equals("head") && !w.equals("tail")) {
        	System.err.println("Incorrect input. Head and Tail only");
        }
        
        else {
            System.out.println("You play " + w );
    		System.out.println("Computer play " + random1);
    		if (w.equals(random1)) {
    			System.out.println("You win");
    		}
    		else {
    			System.out.println("Computer win");
    		}
        }

    	}
       		
      }
}
