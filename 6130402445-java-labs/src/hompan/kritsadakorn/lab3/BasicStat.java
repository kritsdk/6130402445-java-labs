package hompan.kritsadakorn.lab3;
/**
 * BasicStat
 * 
 Author: Kritsadakorn Hompan
 ID: 613040244-5
 Sec: 1
 Date: February 3, 2019
 *
 **/

import java.util.Arrays;

public class BasicStat {
	static double num[];
	static int numInput;


	static double[] acceptInput(String[] args) {
		try {
			numInput = Integer.parseInt(args[0]);
			if (args.length != numInput + 1 || args.length <= 1) { // Check input value
				System.err.println("<BasicStat> <numNumbers> <numbers>...");
				System.exit(0);
			} else {
				num = new double[numInput];
				for (int i = 1; i <= numInput; i++) {
					double number = Double.parseDouble(args[i]);
					num[i - 1] = number;
				}
				Arrays.sort(num);
			}
		} catch (Exception e) {
			System.err.println("<BasicStat> <numNumbers> <numbers>...");
			System.exit(0);
		}

		return num;
	}

	static void displayStats() {
		double sum = 0;
		for (int i = 0; i < num.length; i++) {
			sum += num[i];
		}
		double average = sum / num.length;
		double sd = 0;
		for (int i = 0; i < num.length; i++) {
			sd += Math.pow((num[i] - average), 2) / num.length;
		}
		double standard = Math.sqrt(sd);
		System.out.println("Min is " + num[0] + " Max is " + num[num.length - 1] );
		System.out.println("Average is " + average);
		System.out.println("Standard Deviation is " + standard);

	}

	public static void main(String[] args) {
		acceptInput(args);
		displayStats();
	}

}
