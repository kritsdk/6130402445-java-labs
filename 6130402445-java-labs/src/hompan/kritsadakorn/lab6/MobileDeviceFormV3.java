package hompan.kritsadakorn.lab6;

import java.awt.BorderLayout;
import java.awt.GridLayout;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

public class MobileDeviceFormV3 extends MobileDeviceFormV2 {
	protected JList<?> featList;
	protected JLabel featLapal;
	protected String[] list = {"Design and build quality", "Great Camera", "Screen", "Battery Life"};
	protected JMenuBar  menuBar;
	protected JMenu file,cofig ;
	protected JMenuItem newMI,open,save,exit,color,size;
	protected JPanel listPanel;
	
	
	public MobileDeviceFormV3(String title) {
		super(title);
	}
	
	protected void addComponents() {
		super.addComponents();
		
		newMI = new JMenuItem("New");    
		open = new JMenuItem("Open");    
		save = new JMenuItem("Save");    
		exit = new JMenuItem("Exit");    
		color = new JMenuItem("Color");  
		size = new JMenuItem("Size");   
				
		menuBar = new JMenuBar();    
		file = new JMenu("File");    
		cofig = new JMenu("Cofig");    
    
		file.add(newMI);file.add(open);file.add(save);file.add(size);file.add(exit);
		cofig.add(color);cofig.add(size);
		menuBar.add(file);menuBar.add(cofig);   
		setJMenuBar(menuBar);
		
		featLapal = new JLabel("Features : ");
		featList  = new JList<>(list);
				
		listPanel = new JPanel();
		listPanel.add(featLapal);
		listPanel.add(featList);
		listPanel.setLayout(new GridLayout(0,2));
		textAreaPanel.add(listPanel,BorderLayout.NORTH);

		
	}
	
	protected void setFrameFeatures() {
		pack();
		setLocationRelativeTo(null);
		setVisible(true);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

	}

	public static void createAndShowGUI() {
		MobileDeviceFormV3 mdv3 = new MobileDeviceFormV3("Mobile Device Form V3");
		mdv3.addComponents();
		mdv3.setFrameFeatures();
	}

	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});

	}
}
