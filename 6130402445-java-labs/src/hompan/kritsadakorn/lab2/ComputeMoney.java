package hompan.kritsadakorn.lab2;

/**
* Author: Kritsadakorn Hompan
* ID: 613040244-5
* Sec: 1 
* Date: January 21, 2019
**/

public class ComputeMoney {

	public static void main(String[] args) {
		if (args.length != 4) {
			System.err.println("ComputeMoney <1,000 Bath> <5000 Bath> <100 Bath> <20 Bath>");
		} else {
			float thousand = Float.parseFloat(args[0]);
			float fiveHundred = Float.parseFloat(args[1]);
			float hundred = Float.parseFloat(args[2]);
			float twenty = Float.parseFloat(args[3]);

			float total = (thousand * 1000) + (fiveHundred * 500) + (hundred * 100) + (twenty * 20);

			System.out.println("Total Money is " + total);
		}
	}

}
