package hompan.kritsadakorn.lab2;

/**
* This Java program that accepts three arguments: your favorite football player, the
*  football club that the player plays for, and the nationality of that player. The output of
*  the program is in the format
* "My favorite football player is " + <footballer name>
* "His nationality is " + <nationality>
* "He plays for " + <football club>
*
* Author: Kritsadakorn Hompan
* ID: 613040244-5
* Sec: 1 
* Date: January 21, 2019
*
**/

public class Footballer {

	public static void main(String[] args) {
		if (args.length != 3) {
			System.err.print("Usage : Footballer <footballer name> <nationality> <club name> ");
		} else {
			System.out.print("My favorite football player is " + args[0] + "\n" + "His nationality is " + args[1] + "\n"
					+ "He plays for " + args[2]);

		}
	}
}
