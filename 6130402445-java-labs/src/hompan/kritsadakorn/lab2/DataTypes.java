package hompan.kritsadakorn.lab2;

/** Author: Kritsadakorn Hompan
* ID: 613040244-5
* Sec: 1 
* Date: January 21, 2019
**/

public class DataTypes {

	public static void main(String[] args) {

		String name = new String("Kritsadakorn Hompan"); 
		String ID = new String("6130402445"); 
		char first = name.charAt(0); 
		boolean booleanTrue = true; 
		int twoLastOctal = 055;
		int twoLastHexadecimal = 0x2d;
		long twoLast = 45l;
		float floatTwoLastPointTwoFirst = 45.61f; 
		double doubleTwoLastPointTwoFirst = 45.61d; 
		System.out.println("My name is " + name + "\n" + "My student is " + ID);
		System.out.println(first + " " + booleanTrue + " " + twoLastOctal + " " + twoLastHexadecimal);
		System.out.println(twoLast + " " + floatTwoLastPointTwoFirst + " " + doubleTwoLastPointTwoFirst);
	}

}
