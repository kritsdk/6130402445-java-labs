package hompan.kritsadakorn.lab2;
/**
* Author: Kritsadakorn Hompan
* ID: 613040244-5
* Sec: 1 
* Date: January 21, 2019
**/

import java.util.Arrays;

public class SortNumbers {

	public static void main(String[] args) {
		if (args.length != 5) {
			System.err.println("Input error: enter 5 argument");
		} else {
			float first = Float.parseFloat(args[0]);
			float second = Float.parseFloat(args[1]);
			float third = Float.parseFloat(args[2]);
			float fourth = Float.parseFloat(args[3]);
			float fifth = Float.parseFloat(args[4]);
			Float[] listNumber = { first, second, third, fourth, fifth };
			Arrays.sort(listNumber);
			for (int i = 0; i < listNumber.length; i++) {
				System.out.print(listNumber[i] + " ");
			}
		}
	}
}
