package hompan.kritsadakorn.lab2;

/**
* Author: Kritsadakorn Hompan
* ID: 613040244-5
* Sec: 1 
* Date: January 21, 2019
**/

public class StringAPI {

	public static void main(String[] args) {
		if (args.length != 1) {
			System.err.print("Input error: enter 1 argument");
		} else {
			String enter = new String(args[0]);
			if ((enter.substring(enter.length() - 7, enter.length())).equalsIgnoreCase("college")) {
				System.out.println(enter + " is College");
			} else if ((enter.substring(enter.length() - 10, enter.length())).equalsIgnoreCase("university")) {
				System.out.println(enter + " is university");
			} else {
				System.out.println(enter + " is neither a university nor a college");
			}
		}
	}
}
