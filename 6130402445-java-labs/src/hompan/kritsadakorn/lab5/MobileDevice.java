package hompan.kritsadakorn.lab5;

public class MobileDevice {
	private String modelName;
	private String osName;
	private double price;
	private double weight;
	
		
	public MobileDevice(String modelName,String osName,double price,double weight) {
		this.modelName = modelName;
		this.osName = osName;
		this.price = price;
		this.weight = weight;
	}
	
	public MobileDevice(String modelName,String osName,double price) {
		this.modelName = modelName;
		this.osName = osName;
		this.price = price;
	}
	

	public String getModelName() {
		return modelName;
	}
	public void setModelName(String modelName) {
		this.modelName = modelName;
	}
	public String getOsName() {
		return osName;
	}
	public void setOsName(String osName) {
		this.osName = osName;
	}
	public double getPrice() {
		return price;
	}
	public void setPrice(double price) {
		this.price = price;
	}
	public double getWeight() {
		return weight;
	}
	public void setWeight(double weight) {
		this.weight = weight;
	}


	@Override
	public String toString() {
		return "MobileDevice [Model] Name = " + modelName + ", OS : " + osName + ", Price : " + price + " Baht, Weight=" + weight
				+ "g. ]";
	}
	
	
}
