package hompan.kritsadakorn.lab5;

public class AndroidSmartWatch extends AndroidDevice {

	private String modelName;
	private String brandName;
	private double price;

	public AndroidSmartWatch(String modelName, String brandName, double price) {
		this.modelName = modelName;
		this.brandName = brandName;
		this.price = price;
	}

	public String getModelName() {
		return modelName;
	}

	public void setModelName(String modelName) {
		this.modelName = modelName;
	}

	public String getBrandName() {
		return brandName;
	}

	public void setBrandName(String brandName) {
		this.brandName = brandName;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	@Override
	public String toString() {
		return "AndroidSmartWatch [modelName=" + modelName + ", brandName=" + brandName + ", price=" + price + "]";
	}

	@Override
	void usage() {
		System.out.println("AndroidSmartWatch Usage: Show time, date, your heart rate,and your step count");
		
	
	}

}
