package hompan.kritsadakorn.lab5;

public class SamsungDevice extends MobileDevice {
	private static String brand = "Samsung";
	private double androidVersion;
	

	public SamsungDevice(String modelName, double price,double androidVersion) {
		super(modelName,"Andriod",price);
		this.androidVersion = androidVersion;
		
	}
	
	public SamsungDevice(String modelName, double price, double weight,double androidVersion) {
		super(modelName,"Andriod",price, weight);
		this.androidVersion = androidVersion;
	}

	public static String getBrand() {
		return brand;
	}
	public void setBrand(String brand) {
		SamsungDevice.brand = brand;
	}
	public double getAndroidVersion() {
		return androidVersion;
	}
	public void setAndroidVersion(double androidVersion) {
		this.androidVersion = androidVersion;
		
	}

	@Override
	public String toString() {
		return "SamsungDevice [Model Name : " + super.getModelName() + ", OS : " + super.getOsName() + ", Price : " + super.getPrice() + " Baht, Weight : " + super.getWeight() + " g, Android Version : " + androidVersion + "]";
	}
	

	
	
}
