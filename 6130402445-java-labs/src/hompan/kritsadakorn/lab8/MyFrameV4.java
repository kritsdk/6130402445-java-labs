package hompan.kritsadakorn.lab8;

import javax.swing.SwingUtilities;

public class MyFrameV4 extends MyFrameV3 {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public MyFrameV4(String text) {
		super(text);
	}

	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});
	}

	public static void createAndShowGUI() {
		MyFrameV4 msw = new MyFrameV4("My Frame V4");
		msw.addComponents();
		msw.setFrameFeature();
	}

	protected void addComponents() {
		add(new MyCanvasV4());
	}
}