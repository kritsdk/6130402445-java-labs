package hompan.kritsadakorn.lab8;

import javax.swing.JFrame;
import javax.swing.SwingUtilities;

public class MyFrame extends JFrame{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public MyFrame(String title) {
		super(title);
	}

	public static void createAndShowGUI() {
		MyFrame msw = new MyFrame("My Frame");
		msw.addComponents();
		msw.setFrameFeature();
		
	}
	
	protected void addComponents() {
		add(new MyCanvas());
	}
	
	protected void setFrameFeature() {
		pack();
		setLocationRelativeTo(null);
		setVisible(true);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
	}

	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			
			@Override
			public void run() {
				createAndShowGUI();
			}
		});
		
	}

}
