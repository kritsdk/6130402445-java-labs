package hompan.kritsadakorn.lab8;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;

public class MyCanvasV4 extends MyCanvasV3 {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	MyBrick brick = new MyBrick(0, 0);
	MyBall ball = new MyBall(385, 555);
	MyPedal pedal = new MyPedal(350, 585);;
	Color[] color = { Color.red, Color.orange, Color.yellow, Color.green, Color.cyan, Color.blue, Color.magenta };
	
	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		Graphics2D g2d = (Graphics2D) g;
		g2d.setColor(Color.black);
		g2d.fillRect(0, 0, 800, 600);
		g2d.setColor(Color.white);
		g2d.fill(ball);
		g2d.setColor(Color.gray);
		g2d.fill(pedal);
		
		for (int i = 0; i < 10; i++) {
			for (int j = 0; j < 7; j++) {
				brick.x = 0 + i * 80;
				brick.y = 60 + j * 20;
				g2d.setColor(color[j]);
				g2d.fill(brick);
				g2d.setStroke(new BasicStroke(5));
				g2d.setColor(Color.black);
				g2d.draw(brick);
			}
		}
	}
}
