package hompan.kritsadakorn.lab8;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;

public class MyCanvasV2 extends MyCanvas {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	MyBall ball = new MyBall(385, 285);
	MyPedal pedal = new MyPedal(350, 585);
	MyBrick brick = new MyBrick(360, 0);

	public MyCanvasV2() {
		super();
	}
	@Override
	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		Graphics2D g2d = (Graphics2D) g;
		g2d.setColor(Color.black);
		g2d.fillRect(0, 0, 800, 600);
		g2d.setColor(Color.white);
		g2d.fill(brick);
		g2d.fill(ball);
		g2d.fill(pedal);
		g2d.drawLine(400, 0, 400, 600);
		g2d.drawLine(0, 300, 800, 300);
		
	}
}
