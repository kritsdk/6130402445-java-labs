package hompan.kritsadakorn.lab8;

import javax.swing.SwingUtilities;

public class MyFrameV2 extends MyFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public MyFrameV2(String title) {
		super(title);
	}

	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {

			@Override
			public void run() {
				createAndShowGUI();
			}
		});
	}

	public static void createAndShowGUI() {
		MyFrameV2 msw = new MyFrameV2("My Frame V2");
		msw.addComponents();
		msw.setFrameFeature();
	}

	protected void addComponents() {
		add(new MyCanvasV2());
	}

}
