package hompan.kritsadakorn.lab8;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;

public class MyCanvasV3 extends MyCanvasV2 {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	MyBall ball = new MyBall(0, 0);
	MyBrick brick = new MyBrick(0, 300);

	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		Graphics2D g2d = (Graphics2D) g;
		g2d.setColor(Color.black);
		g2d.fillRect(0, 0, 800, 600);

		for (int i = 0; i < 2; i++) {
			for (int j = 0; j < 2; j++) {
				ball.x = 0 + i * 767;
				ball.y = 0 + j * 567;
				g2d.setColor(Color.white);
				g2d.fill(ball);
			}
		}

		for (int k = 0; k < 10; k++) {
			brick.x = 0 + k * 80;
			g2d.setColor(Color.white);
			g2d.fill(brick);
			g2d.setColor(Color.black);
			g2d.draw(brick);
		}
	}
}
