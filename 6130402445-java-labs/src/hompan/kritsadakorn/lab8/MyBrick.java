package hompan.kritsadakorn.lab8;

import java.awt.geom.Rectangle2D;

public class MyBrick extends Rectangle2D.Double {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	protected final static int brickWidth = 80, brickHeight = 20;

	public MyBrick(int x, int y) {
		super(x, y, brickWidth, brickHeight);

	}

}
