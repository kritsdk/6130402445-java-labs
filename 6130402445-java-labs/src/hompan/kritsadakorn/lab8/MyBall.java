package hompan.kritsadakorn.lab8;

import java.awt.geom.Ellipse2D;

public class MyBall extends Ellipse2D.Double {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	protected final static int diameter = 30;

	public MyBall(int x, int y) {
		super(x, y, diameter, diameter);
	}

}
