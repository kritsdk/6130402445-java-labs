package hompan.kritsadakorn.lab8;

import java.awt.geom.Rectangle2D;

public class MyPedal extends Rectangle2D.Double {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	protected static final int pedalWidth = 100, pedalHeight = 10;

	public MyPedal(int x, int y) {
		super(x, y, pedalWidth, pedalHeight);
	}

}
