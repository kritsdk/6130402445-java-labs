package hompan.kritsadakorn.lab10;
	import java.awt.Color;
	import java.awt.event.ActionEvent;
	import java.awt.event.ActionListener;
	import java.io.File;

	import javax.swing.JColorChooser;
	import javax.swing.JFileChooser;
	import javax.swing.JOptionPane;
	import javax.swing.SwingUtilities;

	public class MobileDeviceFormV9 extends MobileDeviceFormV8 implements ActionListener {
		protected JFileChooser fileChooser;
		protected JColorChooser colorChooser;

		public MobileDeviceFormV9(String title) {
			super(title);
			// TODO Auto-generated constructor stub
		}

		@Override
		public void actionPerformed(ActionEvent event) {
			super.actionPerformed(event);
			Object src = event.getSource();
			fileChooser = new JFileChooser();
			if (src == openMI) {
				int returnval = fileChooser.showOpenDialog(MobileDeviceFormV9.this);

				if (returnval == JFileChooser.APPROVE_OPTION) {
					File file = fileChooser.getSelectedFile();
					JOptionPane.showMessageDialog(this, "Opening file " + file.getName());
				} else {
					JOptionPane.showMessageDialog(this, "Open command cancelled by user ");
				}

			} else if (src == exitMI) {
				System.exit(0);
			} else if (src == saveMI) {
				int returnval = fileChooser.showSaveDialog(MobileDeviceFormV9.this);

				if (returnval == JFileChooser.APPROVE_OPTION) {
					File file = fileChooser.getSelectedFile();
					JOptionPane.showMessageDialog(this, "Saving file " + file.getName());
				} else {
					JOptionPane.showMessageDialog(this, "Save command cancelled by user ");
				}
			} else if (src == redMI) {
				reviewTxtArea.setBackground(Color.RED);
			} else if (src == blueMI) {
				reviewTxtArea.setBackground(Color.BLUE);
			} else if (src == greenMI) {
				reviewTxtArea.setBackground(Color.GREEN);
			} else if (src == customMenu) {
				Color customColor = colorChooser.showDialog(MobileDeviceFormV9.this, "Choose Color", reviewTxtArea.getBackground());
				reviewTxtArea.setBackground(customColor);
			}
		}

		public static void main(String[] args) {
			// TODO Auto-generated method stub
			SwingUtilities.invokeLater(new Runnable() {
				public void run() {
					createAndShowGUI();
				}
			});
		}

		public static void createAndShowGUI() {
			// Mobile Device Form V9
			MobileDeviceFormV9 mobileDeviceFormV9 = new MobileDeviceFormV9("Mobile Device Form V9");
			mobileDeviceFormV9.addComponents();
			mobileDeviceFormV9.addMenus();
			//mobileDeviceFormV9.initComponents();
			mobileDeviceFormV9.setFrameFeatures();
			mobileDeviceFormV9.addListeners();
			mobileDeviceFormV9.addShortcut();
		}

		@Override
		public void addListeners() {
			super.addListeners();
			openMI.addActionListener(this);
			saveMI.addActionListener(this);
			exitMI.addActionListener(this);
			redMI.addActionListener(this);
			greenMI.addActionListener(this);
			blueMI.addActionListener(this);
			customMenu.addActionListener(this);
		}
	}

