package hompan.kritsadakorn.lab10;

import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;

import javax.swing.JMenuItem;
import javax.swing.KeyStroke;
import javax.swing.SwingUtilities;

public class MobileDeviceFormV8 extends MobileDeviceFormV7 {
	protected JMenuItem customMenu;

	public MobileDeviceFormV8(String title) {
		super(title);
		// TODO Auto-generated constructor stub
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});
	}
	public static void createAndShowGUI() {
		// Mobile Device Form V8
		MobileDeviceFormV8 mobileDeviceFormV8 = new MobileDeviceFormV8("Mobile Device Form V8");
		mobileDeviceFormV8.addComponents();
		mobileDeviceFormV8.addMenus();
		//mobileDeviceFormV8.initComponents();
		mobileDeviceFormV8.setFrameFeatures();
		mobileDeviceFormV8.addListeners();
		mobileDeviceFormV8.addShortcut();
	}
	
	@Override
	protected void addMenus() {
		super.addMenus();
		customMenu = new JMenuItem("Custom ...");
		colorMenu.add(customMenu);
	}
	// add mnemonic key
	protected void addShortcut() {
		fileMenu.setMnemonic(KeyEvent.VK_F);
		configMenu.setMnemonic(KeyEvent.VK_C);
		newMI.setMnemonic(KeyEvent.VK_N);
		newMI.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_N,ActionEvent.ALT_MASK));
		openMI.setMnemonic(KeyEvent.VK_O);
		openMI.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_O,ActionEvent.ALT_MASK));
		saveMI.setMnemonic(KeyEvent.VK_S);
		saveMI.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_S,ActionEvent.ALT_MASK));
		exitMI.setMnemonic(KeyEvent.VK_X);
		exitMI.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_X,ActionEvent.ALT_MASK));
		colorMenu.setMnemonic(KeyEvent.VK_L);
		redMI.setMnemonic(KeyEvent.VK_R);
		redMI.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_R,ActionEvent.ALT_MASK));
		greenMI.setMnemonic(KeyEvent.VK_G);
		greenMI.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_G,ActionEvent.ALT_MASK));
		blueMI.setMnemonic(KeyEvent.VK_B);
		blueMI.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_B,ActionEvent.ALT_MASK));
		customMenu.setMnemonic(KeyEvent.VK_U);
		customMenu.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_U,ActionEvent.ALT_MASK));
		
	}
}
