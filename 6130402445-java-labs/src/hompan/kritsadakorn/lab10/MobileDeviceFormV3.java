package hompan.kritsadakorn.lab10;

import java.awt.BorderLayout;
import java.awt.GridLayout;

import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

public class MobileDeviceFormV3 extends MobileDeviceFormV2 {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	protected JList<String> features;
	protected String[] featureValues = { "Design and build quality", "Great Camera", "Screen", "Battery Life" };
	protected JMenuBar menuBar;
	protected JMenu fileMenu, configMenu;
	protected JMenuItem newMI, openMI, saveMI, exitMI, colorMI, sizeMI;
	protected int NUM_VISIBLE_ROWS = 2;
	protected JPanel featuresPanel, comboListPanel;
	protected JLabel featuresLabel;

	public MobileDeviceFormV3(String title) {
		super(title);
	}

	protected void initComponents() {
		super.initComponents();
		features = new JList<String>(featureValues);
		features.setVisibleRowCount(NUM_VISIBLE_ROWS);
		featuresLabel = new JLabel("Features:");
		featuresPanel = new JPanel(new GridLayout(1, 2));
		comboListPanel = new JPanel(new GridLayout(2, 1));
	}

	protected void addComponents() {
		super.addComponents();
		featuresPanel.add(featuresLabel);
		featuresPanel.add(features);
		contentPanel.remove(typePanel);
		comboListPanel.add(typePanel);
		comboListPanel.add(featuresPanel);
		contentPanel.add(comboListPanel, BorderLayout.CENTER);
	}

	protected void addMenus() {
		menuBar = new JMenuBar();
		fileMenu = new JMenu("File");
		configMenu = new JMenu("Config");
		newMI = new JMenuItem("New");
		openMI = new JMenuItem("Open");
		saveMI = new JMenuItem("Save");
		exitMI = new JMenuItem("Exit");
		colorMI = new JMenuItem("Color");
		sizeMI = new JMenuItem("Size");
		fileMenu.add(newMI);
		fileMenu.add(openMI);
		fileMenu.add(saveMI);
		fileMenu.add(exitMI);
		configMenu.add(colorMI);
		configMenu.add(sizeMI);
		menuBar.add(fileMenu);
		menuBar.add(configMenu);
		setJMenuBar(menuBar);
	}

	public static void createAndShowGUI() {
		MobileDeviceFormV3 mobileDeviceForm3 = new MobileDeviceFormV3("Mobile Device Form V3");
		mobileDeviceForm3.addComponents();
		mobileDeviceForm3.addMenus();
		mobileDeviceForm3.setFrameFeatures();
	}

	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});
	}
}
