package hompan.kritsadakorn.lab10;

import java.awt.BorderLayout;
import java.awt.GridLayout;

import javax.swing.ButtonGroup;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;

public class MobileDeviceFormV1 extends MySimpleWindow {
	protected JLabel bNameLabel, mNameLabel, weightLabel, priceLabel, osLabel;
	protected JTextField bNameTxtField, mNameTxtField, weightTxtField, priceTxtField;
	protected JPanel bNamePanel, mNamePanel, weightPanel, pricePanel, osPanel;
	protected JPanel overallPanel, textsPanel, osChoicesPanel;
	protected JRadioButton android, iOS;
	protected ButtonGroup osNames;
	public final static int TXTFIELD_WIDTH = 15;

	public MobileDeviceFormV1(String title) {
		super(title);
	}

	protected void initComponents() {
		bNamePanel = new JPanel(new GridLayout(1, 2));
		mNamePanel = new JPanel(new GridLayout(1, 2));
		weightPanel = new JPanel(new GridLayout(1, 2));
		pricePanel = new JPanel(new GridLayout(1, 2));
		osPanel = new JPanel(new GridLayout(1, 2));
		osChoicesPanel = new JPanel();
		bNameLabel = new JLabel("Brand Name:");
		bNameTxtField = new JTextField(TXTFIELD_WIDTH);
		mNameLabel = new JLabel("Model Name:");
		mNameTxtField = new JTextField(TXTFIELD_WIDTH);
		weightLabel = new JLabel("Weight (kg.):");
		weightTxtField = new JTextField(TXTFIELD_WIDTH);
		priceLabel = new JLabel("Price (Baht):");
		priceTxtField = new JTextField(TXTFIELD_WIDTH);
		osLabel = new JLabel("Mobile OS:");
		android = new JRadioButton("Android");
		iOS = new JRadioButton("iOS");
		osNames = new ButtonGroup();
		osNames.add(android);
		osNames.add(iOS);
		textsPanel = new JPanel(new GridLayout(5, 1));
		overallPanel = new JPanel(new BorderLayout());
	}

	protected void setLabelTxtField(JPanel panel, JLabel label, JTextField txtField) {
		panel.add(label);
		panel.add(txtField);
	}

	protected void addComponents() {
		super.addComponents();
		initComponents();
		setLabelTxtField(bNamePanel, bNameLabel, bNameTxtField);
		setLabelTxtField(mNamePanel, mNameLabel, mNameTxtField);
		setLabelTxtField(weightPanel, weightLabel, weightTxtField);
		setLabelTxtField(pricePanel, priceLabel, priceTxtField);
		osPanel.add(osLabel);
		osChoicesPanel.add(android);
		osChoicesPanel.add(iOS);
		osPanel.add(osLabel);
		osPanel.add(osChoicesPanel);
		textsPanel.add(bNamePanel);
		textsPanel.add(mNamePanel);
		textsPanel.add(weightPanel);
		textsPanel.add(pricePanel);
		textsPanel.add(osPanel);
		overallPanel.add(textsPanel, BorderLayout.NORTH);
		overallPanel.add(buttonsPanel, BorderLayout.SOUTH);
		setContentPane(overallPanel);
	}

	public static void createAndShowGUI() {
		MobileDeviceFormV1 mobileDeviceFormV1 = new MobileDeviceFormV1("Mobile Device Form V1");
		mobileDeviceFormV1.addComponents();
		mobileDeviceFormV1.setFrameFeatures();
	}

	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});
	}
}
