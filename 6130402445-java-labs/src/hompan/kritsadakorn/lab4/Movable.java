package hompan.kritsadakorn.lab4;

public interface Movable {

	public void accelerate();
	
	public void brake();
	
	public void setSpeed(int currentSpeed);
}
