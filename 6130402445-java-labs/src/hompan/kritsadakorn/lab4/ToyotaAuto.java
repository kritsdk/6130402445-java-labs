package hompan.kritsadakorn.lab4;

public class ToyotaAuto extends Automobile implements Movable, Refuelable {
		
	public ToyotaAuto(int maxSpeed,int acceleration,String model) {
		setGasoline(100);
		setMaxSpeed(maxSpeed);
		setAcceleration(acceleration);
		setModel(model);
		
		}

	public void refuel() {
		this.setGasoline(100);	
		System.out.println(getModel() + " refuels");
	}
	
	public void accelerate() {
		int gasoline = getGasoline();
		int currentSpeed = getSpeed();
		int maxSpeed = getMaxSpeed();
		int acceleration = getAcceleration();
		if (currentSpeed + acceleration <= maxSpeed) {
			setSpeed(currentSpeed +  acceleration );
			setGasoline(gasoline -= 15);
			System.out.println(getModel() + " accelerates");
		}
		else {
			setSpeed(maxSpeed);
			setGasoline(gasoline -= 15);
			System.out.println(getModel() + " accelerates");
		}
	}
	
	public void brake() {
		int gasoline = getGasoline();
		int currentSpeed = getSpeed();
		int acceleration = getAcceleration();
		if (currentSpeed-acceleration <= 0) {
			setSpeed(currentSpeed = 0);
			setGasoline(gasoline -= 15);
			System.out.println(getModel() + " brakes");

		}
		else {
			setSpeed(currentSpeed-acceleration);
			setGasoline(gasoline -= 15);
			System.out.println(getModel() + " brakes");
				
		}		
	}
	
	public void setSpeed1(int currentSpeed ) {
		int maxSpeed = getMaxSpeed();

		if (currentSpeed <= 0 ) {
			setSpeed(currentSpeed = 0);	
		}
		
		else if (currentSpeed > maxSpeed) {
			setSpeed(currentSpeed = maxSpeed);
		}
	}

	@Override
	public String toString() {
		return getModel() +" gas: " +getGasoline() + " speed:"+ getSpeed() + " Max Speed: "+ getMaxSpeed() + " Acceleration: "+getAcceleration();
	}
	
	
	
}
