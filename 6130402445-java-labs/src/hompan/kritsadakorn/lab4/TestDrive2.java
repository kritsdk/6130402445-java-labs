package hompan.kritsadakorn.lab4;

public class TestDrive2 {
	public static void main(String[] args) {
		ToyotaAuto car1 = new ToyotaAuto(200,10,"Vios");
		HondaAuto car2 = new HondaAuto(200,8,"City");
		
		System.out.println(car1);
		System.out.println(car2);
		
		car1.accelerate();
		car2.accelerate();
		car2.accelerate();
		
		System.out.println(car1);
		System.out.println(car2);
		
		car1.brake();
		car1.brake();
		car2.brake();
		
		System.out.println(car1);
		System.out.println(car2);
		
		car1.refuel();
		car2.refuel();
		
		System.out.println(car1);
		System.out.println(car2);
		
		isFaster(car1,car2);
		isFaster(car2,car1);
		
		
	}

	public static void isFaster(Automobile car1, Automobile car2) {
		int speedCar1 = car1.getSpeed();
		int speedCar2 = car2.getSpeed();
		
		if (speedCar1 > speedCar2) {
			System.out.println(car1.getModel() + "is faster than " + car2.getModel());
					
		}
		else {
			System.out.println(car1.getModel() + "is NOT faster than " + car2.getModel());
		}
	}
	
	

}
