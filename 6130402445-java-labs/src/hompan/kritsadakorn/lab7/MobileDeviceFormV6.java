package hompan.kritsadakorn.lab7;

import java.awt.BorderLayout;
import java.awt.GridLayout;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

public class MobileDeviceFormV6 extends MobileDeviceFormV5 {
	protected JPanel bottonPanal,reviewPanal;
	public MobileDeviceFormV6(String title) {
		super(title);
	}
	
	protected void addComponents(ReadImage pic) {
		super.addComponents();
		
		reviewPanal = new JPanel(new BorderLayout());
		reviewLabel.add(reviewPanal,BorderLayout.NORTH);
		textArea.add(reviewPanal,BorderLayout.SOUTH);
		
		bottonPanal = new JPanel(new GridLayout());
		bottonPanal.add(cancelButton);
		bottonPanal.add(okButton);
		
		bottomPanel.setLayout(new BorderLayout());
		bottomPanel.add(pic,BorderLayout.CENTER);
		bottomPanel.add(reviewPanal,BorderLayout.NORTH);
		bottomPanel.add(bottonPanal,BorderLayout.SOUTH);
	
	}
	
	protected void addMenus() {
		super.addMenus();

	}

	protected void setFrameFeatures() {
		pack();
		setLocationRelativeTo(null);
		setVisible(true);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

	}

	public static void createAndShowGUI() {
		MobileDeviceFormV6 mdv6 = new MobileDeviceFormV6("Mobile Device Form V6");
		ReadImage pic = new ReadImage();
		mdv6.addComponents(pic);
		mdv6.setFrameFeatures();
		mdv6.addMenus();
		
	}

	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});

	}
	
}
