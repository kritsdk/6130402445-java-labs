package hompan.kritsadakorn.lab7;

import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.net.URL;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

public class MobileDeviceFormV4 extends MobileDeviceFormV3 {
	protected JMenuItem redColor,greenColor,blueColor,sixtSize,twntSize,twntfourSize ;
	protected JPanel bottonPanal,reviewPanal;
	public MobileDeviceFormV4(String title) {
		super(title);
	}
	
	protected void updateMenuIcon() {
         URL myIcon = this.getClass().getResource("/new.jpg");
         newMI.setIcon(new ImageIcon(myIcon));

	}
	
	protected void addComponents() {
		super.addComponents();

	}
	
	protected void addSubMenus() {
		cofig.remove(color);
		cofig.remove(size);
		
		color = new JMenu("Color");
		size = new JMenu("Size");
		
		redColor = new JMenuItem("Red");
		greenColor = new JMenuItem("Green");
		blueColor = new JMenuItem("Blue");
		sixtSize = new JMenuItem("16");
		twntSize = new JMenuItem("20");
		twntfourSize = new JMenuItem("24");
		
		color.add(redColor);color.add(greenColor);color.add(blueColor);
		size.add(sixtSize);size.add(twntSize);size.add(twntfourSize);
		
		cofig.add(color);cofig.add(size);
		
	}
	
	protected void addMenus() {
		super.addMenus();
		addSubMenus();
		updateMenuIcon();
	 
	}

	protected void setFrameFeatures() {
		pack();
		setLocationRelativeTo(null);
		setVisible(true);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

	}

	public static void createAndShowGUI() {
		MobileDeviceFormV4 mdv4 = new MobileDeviceFormV4("Mobile Device Form V4");
		mdv4.addComponents();
		mdv4.setFrameFeatures();
		mdv4.addMenus();

		
	}

	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});

	}
}
