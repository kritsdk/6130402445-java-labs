package hompan.kritsadakorn.lab3;
/**
 * TypingTest
 * 
 Author: Kritsadakorn Hompan
 ID: 613040244-5
 Sec: 1
 Date: February 3, 2019
 *
 **/

import java.util.Scanner;

public class TypingTest {
	static String randomColor() {
		String[] color = { "YELLOW", "RED", "BLUE", "INDIGO", "VIOLET", "GREEN", "ORANGE" };
		String colorAll = "";
		for (int i = 0; i < 8; i++) {
			int rand = (int) (Math.random() * 7);
			colorAll += color[rand] + " ";
		}
		colorAll = colorAll.substring(0, colorAll.length() - 1);
		return colorAll;

	}


	static void display(String colorAll) {
		System.out.println(colorAll);
		double time = System.currentTimeMillis();
		while (true) {
			System.out.print("Typeyour answer: ");
			Scanner sc = new Scanner(System.in);
			String enter = sc.nextLine();
			if (enter.equalsIgnoreCase(colorAll)) {
				double timeEnd = (System.currentTimeMillis() - time) / 1000;
				System.out.println("Your time is: " + timeEnd + ".");
				if (timeEnd < 12.0) {
					System.out.println("You type faster than average person.");
					sc.close();
					System.exit(0);
				} else {
					System.out.println("You type slower than average person.");
					sc.close();
					System.exit(0);
				}
			}
		}

	}

	public static void main(String[] args) {
		display(randomColor());
	}

}