package hompan.kritsadakorn.lab3;
/**
 * MatchingPennyMethod
 * 
 Author: Kritsadakorn Hompan
 ID: 613040244-5
 Sec: 1
 Date: February 3, 2019
 *
 **/

import java.util.Random;
import java.util.Scanner;

public class MatchingPennyMethod {

    public static String acceptInput() {
        Scanner sc = new Scanner(System.in);
        System.out.print("Enter Head or Tail : ");
        String input = sc.nextLine();
        String write = input.toLowerCase();
        if (write.equals("exit") ) {
        	System.out.println("Good Bye");
        	System.exit(0);
        }
        else if (write.equals("head") ) {
        	System.out.println(" You play head ");
        }
        else if (write.equals("tail")) {
            System.out.println("You play tail " );
        }
        else {
            System.err.println("Incorrect input. Head and Tail only");
            acceptInput();
        }
        return input;
    }
    public static String genComChoice() {
        Random random = new Random();
        int random_HT = random.nextInt(2);
        String random1;
		if (random_HT == 0) {
            random1 = "head";
            System.out.println( "Computer play head" );
        }
        else {
            random1 = "tail";
            System.out.println( "Computer play tail" );
            
        }
        return random1;
    }

    public static void displayWiner(String fromUser,String fromCom) {
        if (fromUser.equals(fromCom)) {
            System.out.println("You win");
        }
        else {
            System.out.println("Computer win");
        }
                   
    }

    public static void main(String[] args) {
        while (true) {
            String user = acceptInput();
            String com = genComChoice();
			displayWiner(user, com);
        } 
    }
}