package hompan.kritsadakorn.lab6;

import java.awt.FlowLayout;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

public class MySimpleWindow extends JFrame {
	protected JButton cancelButton;
	protected JButton okButton;
	protected JPanel bottonPanel;
	protected JPanel windowsPanel;

	public MySimpleWindow(String title) {
		super(title);
	}
//
	protected void addComponents() {
		cancelButton = new JButton("Cancel");
		okButton = new JButton("OK");

		bottonPanel = new JPanel();
		bottonPanel.add(cancelButton);
		bottonPanel.add(okButton);

		windowsPanel = new JPanel();
		windowsPanel = (JPanel) this.getContentPane();
		windowsPanel.setLayout(new FlowLayout());

		windowsPanel.add(bottonPanel);
	}

	protected void setFrameFeatures() {
		pack();
		setLocationRelativeTo(null);
		setVisible(true);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

	}

	public static void createAndShowGUI() {
		MySimpleWindow msw = new MySimpleWindow("My Simple Window");
		msw.addComponents();
		msw.setFrameFeatures();
	}

	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});

	}

}
