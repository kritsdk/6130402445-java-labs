package hompan.kritsadakorn.lab6;

import java.awt.BorderLayout;
import java.awt.GridLayout;

import javax.swing.ButtonGroup;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;

public class MobileDeviceFormV1 extends MySimpleWindow {

	protected JLabel brandLabel, modelLabel, weightLabel, priceLabel, mobileLabel;
	protected JTextField brandTxtField, modelTxtField, weightTxtField, priceTxtField;
	protected JPanel topPanel,radioPanel;
	protected ButtonGroup groupRadio;
	protected JRadioButton android,iOS ;
	
	public MobileDeviceFormV1(String title) {
		super(title);
	}

	protected void addComponents() {
		super.addComponents();
		windowsPanel = (JPanel) this.getContentPane();
		
		brandLabel = new JLabel(" Brand Name : ");
		brandTxtField = new JTextField(15);
		modelLabel = new JLabel(" Model Name : ");
		modelTxtField = new JTextField(15);
		weightLabel = new JLabel(" Weight(kg.) : ");
		weightTxtField = new JTextField(15);
		priceLabel = new JLabel(" Price(Baht) : ");
		priceTxtField= new JTextField(15);
		mobileLabel = new JLabel(" Mobile OS : ");
		android = new JRadioButton("Android");
		iOS = new JRadioButton("iOS");
		

		groupRadio = new ButtonGroup();
		groupRadio.add(android);
		groupRadio.add(iOS);
		
		radioPanel = new JPanel();
		radioPanel.add(android);
		radioPanel.add(iOS);
		
		
		topPanel = new JPanel();
		topPanel.setLayout(new GridLayout(0,2));
		
		topPanel.add(brandLabel);
		topPanel.add(brandTxtField);
		topPanel.add(modelLabel);
		topPanel.add(modelTxtField);
		topPanel.add(weightLabel);
		topPanel.add(weightTxtField);
		topPanel.add(priceLabel);
		topPanel.add(priceTxtField);
		topPanel.add(mobileLabel);
		topPanel.add(radioPanel);
		
		windowsPanel = new JPanel();
		windowsPanel = (JPanel) this.getContentPane();
		windowsPanel.setLayout(new BorderLayout());
		windowsPanel.add(topPanel, BorderLayout.NORTH);
		windowsPanel.add(bottonPanel, BorderLayout.SOUTH);

	}

	protected void setFrameFeatures() {
		pack();
		setLocationRelativeTo(null);
		setVisible(true);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

	}

	public static void createAndShowGUI() {
		MobileDeviceFormV1 mdv1 = new MobileDeviceFormV1("Mobile Device Form V1");
		mdv1.addComponents();
		mdv1.setFrameFeatures();
	}

	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});

	}

}
