package hompan.kritsadakorn.lab7;

import java.awt.BorderLayout;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.SwingUtilities;

public class MobileDeviceFormV2 extends MobileDeviceFormV1 {
	protected JLabel typeLabel,reviewLabel;
	protected JComboBox<?> typebox;
	protected JTextArea textArea;
	protected String[] type = {"Phone", "Tablet", "Smart TV"};
	protected JPanel centerPanel;
		
	public MobileDeviceFormV2(String title) {
		super(title);
	}
	
	protected void addComponents() {
		super.addComponents();
		
		typebox = new JComboBox<>(type);
		typeLabel = new JLabel("Type : ");
		reviewLabel = new JLabel("Review : ");
		
		textArea = new JTextArea(2,35);	
		JScrollPane scrollingArea = new JScrollPane(textArea);
		textArea.setText("Bigger than previous Note phones in every way, the Samsung\n Galaxy Note 9 has a larger 6.4-inch screen, heftier 4,000mAh\n battery, and a massive 1TB of storage option. ");
		
		centerPanel = new JPanel();
		centerPanel.setLayout(new BorderLayout());
		centerPanel.add(scrollingArea,BorderLayout.SOUTH);
		centerPanel.add(reviewLabel,BorderLayout.CENTER);
		
		windowsPanel.add(centerPanel,BorderLayout.CENTER);
		topPanel.add(typeLabel);
		topPanel.add(typebox);
		
	}
	
	
	protected void setFrameFeatures() {
		pack();
		setLocationRelativeTo(null);
		setVisible(true);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

	}

	public static void createAndShowGUI() {
		MobileDeviceFormV2 mdv2 = new MobileDeviceFormV2("Mobile Device Form V2");
		mdv2.addComponents();
		mdv2.setFrameFeatures();
	}

	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});

	}

}
