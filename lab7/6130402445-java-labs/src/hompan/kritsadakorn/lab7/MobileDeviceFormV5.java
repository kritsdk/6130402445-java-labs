package hompan.kritsadakorn.lab7;

import java.awt.Color;
import java.awt.Font;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

public class MobileDeviceFormV5 extends MobileDeviceFormV4 {

	public MobileDeviceFormV5(String title) {
		super(title);
	}
	
	protected void addComponents() {
		super.addComponents();
		
	}
	protected void  initComponents() {
		brandLabel.setFont(new Font("Serif",Font.PLAIN,14));
		modelLabel.setFont(new Font("Serif",Font.PLAIN,14));
		weightLabel.setFont(new Font("Serif",Font.PLAIN,14));
		priceLabel.setFont(new Font("Serif",Font.PLAIN,14));
		mobileLabel.setFont(new Font("Serif",Font.PLAIN,14));
		typeLabel.setFont(new Font("Serif",Font.PLAIN,14));
		featLapal.setFont(new Font("Serif",Font.PLAIN,14));
		reviewLabel.setFont(new Font("Serif",Font.PLAIN,14));
		
		brandTxtField.setFont(new Font("Serif",Font.BOLD,14));
		modelTxtField.setFont(new Font("Serif",Font.BOLD,14));
		weightTxtField.setFont(new Font("Serif",Font.BOLD,14));
		priceTxtField.setFont(new Font("Serif",Font.BOLD,14));
		textArea.setFont(new Font("Serif",Font.BOLD,14));
	
		okButton.setForeground(Color.BLUE);
		cancelButton.setForeground(Color.RED);
	}
	
	protected void addMenus() {
		super.addMenus();
		initComponents();
	}

	protected void setFrameFeatures() {
		pack();
		setLocationRelativeTo(null);
		setVisible(true);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

	}

	public static void createAndShowGUI() {
		MobileDeviceFormV5 mdv5 = new MobileDeviceFormV5("Mobile Device Form V5");
		mdv5.addComponents();
		mdv5.setFrameFeatures();
		mdv5.addMenus();
		
	}

	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});

	}
	
	
}
